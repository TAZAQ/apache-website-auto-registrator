import sys
from PyQt5 import QtWidgets
from MainForm import MainForm

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainForm = MainForm()

    desktop = QtWidgets.QApplication.desktop()
    x_mid, y_mid = (desktop.width() - mainForm.width()) // 2, (desktop.height() - mainForm.height()) // 2
    mainForm.move(x_mid, y_mid)

    mainForm.show()
    app.exec_()
