# Авторегистратор сайтов в Apache2
Данное приложение создаёт всё что нужно для нового web-проекта Apache2


## Установка
| Название инструкции | Команда |
| ------ | ------ |
| Клонировать репозиторий | `https://gitlab.com/TAZAQ/apache-website-auto-registrator.git` |
| Создать виртуальное окружение, пусть будет такое | `python3 -m venv awar` | 
| Активировать окружение | `source awar/bin/activate` | 
| Установить зависимости из requirements.txt | `pip3 install -r requirements.txt` | 

## Запуск приложения

`sudo python3 main.py`

![](Images/Application.png)

## Как использовать
1. Ввести название вашего web-проекта в поле ввода "Website name", **НЕ** используйте символ "_"
2. Выбрать папку, в котором будет расположен ваш web-проект, нажав на кнопку **Choose directory**
3. Нажать на кнопку **Register in Apache**


После выполнения всех действий, проект будет доступен в браузере по адресу `yoursite.test`