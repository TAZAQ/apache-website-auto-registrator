# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainForm.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowModality(QtCore.Qt.WindowModal)
        MainWindow.resize(900, 750)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(900, 750))
        MainWindow.setMaximumSize(QtCore.QSize(900, 750))
        MainWindow.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.website_name_edit = QtWidgets.QLineEdit(self.centralwidget)
        self.website_name_edit.setText("")
        self.website_name_edit.setObjectName("website_name_edit")
        self.verticalLayout_2.addWidget(self.website_name_edit)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.apache_config_text = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.apache_config_text.setEnabled(True)
        self.apache_config_text.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.apache_config_text.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.apache_config_text.setReadOnly(True)
        self.apache_config_text.setObjectName("apache_config_text")
        self.verticalLayout_2.addWidget(self.apache_config_text)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_2.addWidget(self.label_3)
        self.home_directory_edit = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.home_directory_edit.sizePolicy().hasHeightForWidth())
        self.home_directory_edit.setSizePolicy(sizePolicy)
        self.home_directory_edit.setObjectName("home_directory_edit")
        self.verticalLayout_2.addWidget(self.home_directory_edit)
        self.btn_choose_dir = QtWidgets.QPushButton(self.centralwidget)
        self.btn_choose_dir.setObjectName("btn_choose_dir")
        self.verticalLayout_2.addWidget(self.btn_choose_dir)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_2.addWidget(self.label_4)
        self.index_page_text = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.index_page_text.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.index_page_text.sizePolicy().hasHeightForWidth())
        self.index_page_text.setSizePolicy(sizePolicy)
        self.index_page_text.setReadOnly(True)
        self.index_page_text.setObjectName("index_page_text")
        self.verticalLayout_2.addWidget(self.index_page_text)
        self.btn_register_in_apache = QtWidgets.QPushButton(self.centralwidget)
        self.btn_register_in_apache.setObjectName("btn_register_in_apache")
        self.verticalLayout_2.addWidget(self.btn_register_in_apache)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_3.addWidget(self.label_5)
        self.logs_text = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.logs_text.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.logs_text.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.logs_text.setObjectName("logs_text")
        self.verticalLayout_3.addWidget(self.logs_text)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        self.verticalLayout.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 900, 22))
        self.menubar.setObjectName("menubar")
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        self.menuClear_All = QtWidgets.QMenu(self.menubar)
        self.menuClear_All.setObjectName("menuClear_All")
        MainWindow.setMenuBar(self.menubar)
        self.action_exit = QtWidgets.QAction(MainWindow)
        self.action_exit.setObjectName("action_exit")
        self.actionClear_All = QtWidgets.QAction(MainWindow)
        self.actionClear_All.setObjectName("actionClear_All")
        self.menu.addAction(self.action_exit)
        self.menuClear_All.addAction(self.actionClear_All)
        self.menubar.addAction(self.menu.menuAction())
        self.menubar.addAction(self.menuClear_All.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Apache website auto registrator"))
        self.label.setText(_translate("MainWindow", "Website name (don`t use \"_\")"))
        self.website_name_edit.setPlaceholderText(_translate("MainWindow", "example"))
        self.label_2.setText(_translate("MainWindow", "Apache2 Config ( {0} - is website name )"))
        self.apache_config_text.setPlainText(_translate("MainWindow", "<VirtualHost *:80>\n"
"ServerName {0}.test\n"
"ServerAdmin webmaster@localhost\n"
"DocumentRoot /var/www/{0}/\n"
"ErrorLog ${APACHE_LOG_DIR}/error.log\n"
"CustomLog ${APACHE_LOG_DIR}/access.log combined\n"
"</VirtualHost>"))
        self.label_3.setText(_translate("MainWindow", "Home directory"))
        self.home_directory_edit.setPlaceholderText(_translate("MainWindow", "/home/user/site"))
        self.btn_choose_dir.setText(_translate("MainWindow", "Choose directory"))
        self.label_4.setText(_translate("MainWindow", "Index page text ( {0} - is website name )"))
        self.index_page_text.setPlainText(_translate("MainWindow", "<!DOCTYPE html>\n"
"<html lang=\"ru\">\n"
"<head>\n"
"<meta charset=\"UTF-8\">\n"
"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
"<title>{0}</title>\n"
"<link rel=\"stylesheet\" href=\"styles.css\">\n"
"</head>\n"
"<body>\n"
"Welcome to {0}.test!\n"
"</body>\n"
"</html>"))
        self.index_page_text.setPlaceholderText(_translate("MainWindow", "<VirtualHost *:80> ServerName $site_name.test ServerAdmin webmaster@localhost DocumentRoot /var/www/{0}/ ErrorLog \\${APACHE_LOG_DIR}/error.log CustomLog \\${APACHE_LOG_DIR}/access.log combined </VirtualHost>"))
        self.btn_register_in_apache.setText(_translate("MainWindow", "Register in Apache"))
        self.label_5.setText(_translate("MainWindow", "Logs (show after click on Register button)"))
        self.menu.setTitle(_translate("MainWindow", "File"))
        self.menuClear_All.setTitle(_translate("MainWindow", "Edit"))
        self.action_exit.setText(_translate("MainWindow", "Exit"))
        self.actionClear_All.setText(_translate("MainWindow", "Clear All"))
