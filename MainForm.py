from PyQt5 import QtWidgets
import mainFormQt
import os
import subprocess

registration_confirmation_text = "Confirmation of registration"
confirmation_template_text = """
Your site "{0}" will be located in the directory:
"{1}"

Do you agree?"""

my_virtual_host_template = """<VirtualHost *:80>
ServerName %s.test
ServerAdmin webmaster@localhost
DocumentRoot /var/www/%s/
ErrorLog ${APACHE_LOG_DIR}/error.log
CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
"""

index_page_text_template = """<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{0}</title>
<link rel="stylesheet" href="styles.css">
</head>
<body>
Welcome to {0}.test!
</body>
</html>
"""


class MainForm(QtWidgets.QMainWindow, mainFormQt.Ui_MainWindow):
    # Form initialization
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.__connect_methods()

    def __connect_methods(self):
        # action Exit
        self.action_exit.triggered.connect(exit)
        # action Clear All
        self.actionClear_All.triggered.connect(self.__clear_all_fields)
        # website_name_edit text change event
        self.website_name_edit.textChanged.connect(self.__change_apache_config)
        # button Choose directory
        self.btn_choose_dir.clicked.connect(self.__browse_folder)
        # button Register in Apache
        self.btn_register_in_apache.clicked.connect(self.__register_in_apache)

    def __clear_all_fields(self):
        self.website_name_edit.setText("")
        self.apache_config_text.setPlainText(my_virtual_host_template)
        self.home_directory_edit.setText("")
        self.index_page_text.setPlainText(index_page_text_template)

    def __change_apache_config(self):
        site_name = self.website_name_edit.text()
        self.apache_config_text.setPlainText(my_virtual_host_template % (site_name, site_name))
        self.index_page_text.setPlainText(index_page_text_template.format(site_name))

    def __browse_folder(self):
        self.directory = QtWidgets.QFileDialog.getExistingDirectory()
        if self.directory:
            self.home_directory_edit.setText(self.directory)

    def __register_in_apache(self):
        site_name = self.website_name_edit.text()
        directory = self.home_directory_edit.text()
        full_path = directory + "/" + site_name + "/"

        if site_name == "" or not os.path.exists(directory):
            self.__message_box_warning("Field Error", "Website name is empty or directory is not exist")

        # confirm user's actions
        reply = self.__message_box_question(
            registration_confirmation_text,
            confirmation_template_text.format(site_name, full_path)
        )

        if reply == QtWidgets.QMessageBox.No:
            return

        # registration process:
        # 1) making site directory
        # 2) adding site to hosts
        # 3) adding site to /etc/apache2/sites-available/
        # 4) making link on site path in /var/www/
        # 5) making index.php in site directory
        self.__log_clear()
        self.__log_add(f"Site name: {site_name}")
        self.__log_add(f"Site path: {full_path}")

        # 1) making site directory
        try:
            self.__log_add("Creating path", is_first=True)
            os.makedirs(full_path)
            self.__log_add("Path created")
        except Exception as err:
            self.__log_add(f"Error: {str(err)}")
            return

        # 2) adding site to hosts
        try:
            self.__log_add("Trying add the site to hosts:", is_first=True)
            host_string = f"\n127.0.0.1 {site_name}.test"
            self.__log_add(host_string)
            with open("/etc/hosts", "a") as f:
                f.writelines([host_string])
            self.__log_add("Success")
        except Exception as err:
            self.__log_add(f"Error: {str(err)}")
            return

        # 3) adding site to /etc/apache2/sites-available/
        try:
            self.__log_add("Trying add apache configuration", is_first=True)
            with open(f"/etc/apache2/sites-available/{site_name}.conf", "w") as f:
                f.writelines([my_virtual_host_template % (site_name, site_name)])
            self.__log_add("Trying enabled apache configuration")
            subprocess.call(["a2ensite", f"{site_name}.conf"])
            self.__log_add("Reloading apache2")
            subprocess.call(["systemctl", "reload", "apache2"])
            self.__log_add("Success")
        except Exception as err:
            self.__log_add(f"Error: {str(err)}")
            return

        # 4) making link on site path in /var/www/
        try:
            self.__log_add("Trying create link on site directory in var", is_first=True)
            subprocess.call(["ln", "-s", full_path, "/var/www/"])
            self.__log_add("Success")
        except Exception as err:
            self.__log_add(f"Error: {str(err)}")
            return

        # 5) making index.php in site directory
        try:
            self.__log_add("Trying make index.php in site directory", is_first=True)
            with open(f"{full_path}index.php", "a") as f:
                f.writelines([index_page_text_template.format(site_name)])
            self.__log_add("Yeee, now change user rights")
            subprocess.call(["chmod", "ugo+rwx", full_path])
            subprocess.call(["chmod", "ugo+rwx", full_path + "index.php"])
            self.__log_add("Success")
        except Exception as err:
            self.__log_add(f"Error: {str(err)}")
            return

    def __message_box_warning(self, title, text):
        QtWidgets.QMessageBox.warning(self, title, text, QtWidgets.QMessageBox.Ok)

    def __message_box_question(self, title, text, is_no=True):
        active_button = QtWidgets.QMessageBox.Yes
        if is_no:
            active_button = QtWidgets.QMessageBox.No
        return QtWidgets.QMessageBox.question(self, title, text,
                                              QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, active_button)

    def __log_add(self, text: str, is_first=False):
        if is_first:
            self.logs_text.appendPlainText("-------------------------------------")
            self.logs_text.appendPlainText("")
        self.logs_text.appendPlainText(text)

    def __log_clear(self):
        self.logs_text.clear()
